## Learn Git
> ***Configuration***
```
Config User
$ git config --global user.name "Your Name"
$ git config --global user.name "email@example.com"
```

> ***Create***
```
Create a new local repository
$ git init

Clone an existing repository
$ git clone https://gitlab.com/armin-lau/learn-git
```

> ***Local Operation***
```
Add a file to repository
$ git add file

Submit changes to repository
$ git commit -m "relevant comment"

Push content in local repository to remote repository
$ git push -u origin master
```

> ***Branch Management***
```
Create a new branch
$ git branch branchName

Switch to other branch
$ git checkout branchName

Create a new branch and switch to it
$ git checkout -b branchName

Check current branch
$ git branch

Merge a branch to master
$ git merge branchName

Delete a branch
$ git branch -d branchName
